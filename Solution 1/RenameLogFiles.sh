#!/bin/bash

# Rename all the files last to first to ensure no files get overwritten.  
for (( i=999;i>0;i-- )) do
   j=$((i + 1))

   mv tomcat.$i tomcat.$j
   echo $i $j
done

# Remove the last file as it is no longer needed.
if [ -f tomcat.1000 ]; then
   rm tomcat.1000
fi

