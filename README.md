# Hector Aguilar's Okta Scripting Test

## *** Not intended for production use ***

## Instructions

You have a directory on a remote server that stores Tomcat log files. They follow the format below.

``logs/tomcat.001``
``logs/tomcat.002``
``logs/tomcat.003``
``logs/tomcat.004``
``logs/tomcat.005``
``logs/tomcat.006``
``...``
``logs/tomcat.999``

The most recent log file is tomcat.001.
The oldest log file is tomcat.999. 

Create a script to collect the new log file from the directory "./received/" and place it in the "./logs/" directory.
Only 1000 log files should be kept.


### Unknowns
1. How often a new log file will be available?
2. If there will always be 1000 log files.
3. What happens if a log file is missing.

### Assumptions
1. This script will be run regularly via cron.

### Script Requirements
1. Collect the latest log file named tomcat.log from received/ and place it in logs/.
2. Do not corrupt or overwrite log files.
3. Only 1000 (Or 999 in this case) log files should be kept.

## Solutions
The contents of this Git repository contain 2 solutions to this challenge. 
Each solution contains 2 scripts. One script to create test data, and one script to perform the task.

1. Solution 1 - These scripts were created in about 45 minutes after the end of my interview. I created these as I really was not happy about my performance during the interview. It really just shows the basic processes required.
2. Solution 2 - This is a more complete solution to the requirements. It also adds a layer of error checking to deal with unexpected behaviour. 
3. Solution 3 - This is almost idential to Solution 2. It just has an additional check to enable the script to be used in the cron job. The check compares if ./received/tomcat.log and ./logs/tomcat.log are the same. If they are the script with terminate with a warning.

### Conclusion
At the time it seemed that this was a fairly simple challenge, but I have iterated it further as would happen in real life. This is a form of Continual Service Improvement (CSI).


