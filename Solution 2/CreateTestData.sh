#!/bin/bash
# Written by Chris Woollard - 24th November 2018
# Version 2.0

# Create Directry Structure if it doesn't exist.
if ! [ -d received ]; then
   mkdir received
fi

if ! [ -d logs ]; then
   mkdir logs
fi

# Create Test Data if it doesn't already exist.
for (( i=001;i<1000;i++ )) do
   if [ ! -e "logs/tomcat.$i" ]; then
      touch "./logs/tomcat.$i"
   fi
done

# Create source logfile ready to copy.
if [ ! -f received/tomcat.log ]; then
   touch received/tomcat.log
fi

# Remove a logfile from the logs directory so a missing logfile test can occur.
if [ -f logs/tomcat.500 ]; then
   rm ./logs/tomcat.500
fi

#End

