#!/bin/bash
# Written by Chris Woollard - 24th November 2018
# Version 2.0

###########################################################################
### Add Current Date & Time to ./recieved/tomcat.log for Test Purposes. ###
###########################################################################
date > ./received/tomcat.log
###########################################################################

# Remove the last file if it exists as it is no longer needed.
if [ -f ./logs/tomcat.1000 ]; then
   rm ./logs/tomcat.1000
fi

# Rename all the files last to first to ensure no files get overwritten.  
for (( i=999;i>0;i-- )) do
   j=$((i + 1))
   if [ -e ./logs/tomcat.$i ]; then
      mv -f ./logs/tomcat.$i ./logs/tomcat.$j
      echo $i $j
   else
      echo "Warning - tomcat.$i does not exist."
   fi
done

# Remove the last file as it is no longer needed. Recreated in previous step.
if [ -f ./logs/tomcat.1000 ]; then
   rm ./logs/tomcat.1000
fi

# Move received log file into logs directory.
if [ ! -e /logs/tomcat.1 ]; then
   cp -f ./received/tomcat.log ./logs/tomcat.1
else
   echo "Error - Cannot move file! tomcat.1 already exists\n"
fi
